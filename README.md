This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## React task
After finish with the task in Vue.js I tried to lear React and Redux, React and Vue are very similar, but React and Vuex do not.
For this task I created two git branches, in "vanillaReact" branch I tried to implement the task without any state management system but it wasn't possible so I keep working in the master branch with Redux. After reading Redux documentation and analysing examples I realized this state management architecture is a lot more complex than Vuex and I would need at least one week to learn how to fetch AJAX data to the store, normalize data and make it work along with my React app in the right way.
  
