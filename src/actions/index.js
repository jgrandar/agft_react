export const RECEIVE_EVENTS = 'RECEIVE_EVENTS'

function receiveEvents(json) {
  return {
    type: RECEIVE_EVENTS,
    events: json,
  }
}

function fetchEvents() {
  return dispatch => {
    return fetch('http://www.mocky.io/v2/59f08692310000b4130e9f71')
      .then(response => response.json())
      .then(json => dispatch(receiveEvents(json)))
  }
}

export const addBet = bet => ({
  type: ADD_BET,
  id: bet.id,
});
