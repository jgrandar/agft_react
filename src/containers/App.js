import React, { Component } from 'react'
import FontAwesome from 'react-fontawesome'
import '../index.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="header">
          <div className="burger">
            <FontAwesome className='burger__icon' name='bars' size='2x'/>
          </div>
        </header>
        <main className="event-list">
          {events}
        </main>
      </div>
    )
  }
}
