import React, { Component } from 'react'
// import Market from './Market';

class Event extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render () {
    if (this.props.event.markets.length > 0) {
      return (
        <div className="event">
          <div className="event__header">
            <span>{this.props.event.name}</span>
          </div>
          <div className="event__body">
          </div>
        </div>
      )
    }

    return null;
  }
}

export default Event
